package com.example.internationalizingexamplecode

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.internationalizingexamplecode.ui.theme.InternationalizingExampleCodeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            InternationalizingExampleCodeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    IntentDemo()
                }
            }
        }
    }
}


@Composable
fun IntentDemo ( context: Context = LocalContext.current) {
        
    var userInput by rememberSaveable { mutableStateOf("") }
    val onUserInputChange = {text : String -> userInput = text}

    Column (
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Button(onClick = {
            val intent = Intent(context, AnotherActivity::class.java)
            context.startActivity(intent)
        } ) {
            Text(stringResource(R.string.start_activity))
        }

        Spacer(modifier = Modifier.height(40.dp))

        Button(onClick = {
            Intent(Intent.ACTION_MAIN).also { it.`package`="com.google.android.youtube"
                try {
                    context.startActivity(it)
                } catch (e:ActivityNotFoundException) {
                    e.printStackTrace()
                }
            }
        }) {
            Text(stringResource(R.string.start_youtube))
        }

        Spacer(modifier = Modifier.height(40.dp))

        Button(onClick = {
            val intent = Intent(Intent.ACTION_SEND).apply {
                type = "text/plain"
                putExtra(Intent.EXTRA_EMAIL, arrayListOf("yourAddress@gmail.com"))
                putExtra(Intent.EXTRA_SUBJECT, "Subject of the email")
                putExtra(Intent.EXTRA_TEXT, "This is the body of the email")
            }
            if(intent.resolveActivity(context.packageManager) != null) {
                context.startActivity(intent)
            }
        }) {
            Text(text = stringResource(R.string.send_email))
        }

        Spacer(modifier = Modifier.height(40.dp))

        OutlinedButton(onClick = {
            val webIntent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://gitlab.com/crdavis/intentsexamplecode"))
            context.startActivity(webIntent)
        }) {
            Text(text = stringResource(R.string.open_website))
        }

       Spacer(modifier = Modifier.height(40.dp))

        GetUserInput(title = stringResource(R.string.enter_your_name), textState = userInput,
            onTextChange = onUserInputChange)

        if(userInput != "") {
             Spacer(modifier = Modifier.height(40.dp))
            Text(text = stringResource(R.string.greetings_to, userInput),
                fontWeight = FontWeight.Bold, fontSize = 20.sp, color = Color.Blue
            )
        }

    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GetUserInput (title: String, textState: String, onTextChange: (String) -> Unit)
{
    OutlinedTextField(value = textState, onValueChange = { onTextChange(it)},
    singleLine = true,
        label = { Text(title) }
    )
}


@Preview(showBackground = true, locale = "fr")
@Composable
fun GreetingPreview() {
    InternationalizingExampleCodeTheme {
        IntentDemo()
    }
}